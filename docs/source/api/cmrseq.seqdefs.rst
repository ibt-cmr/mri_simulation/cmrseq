Composite Sequence Definitions
===============================

**Import**: cmrseq.seqdefs

.. automodule:: cmrseq.parametric_definitions
   :members:
   :undoc-members:
   :show-inheritance:


.. toctree::
   :maxdepth: 2
   :hidden:

   cmrseq.seqdefs.excitation
   cmrseq.seqdefs.readout
   cmrseq.seqdefs.velocity
   cmrseq.seqdefs.diffusion
   cmrseq.seqdefs.sequences
