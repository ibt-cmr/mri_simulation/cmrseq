Building Blocks
===================

.. image:: ../_static/cmrseq_bausteine_uml.png
   :scale: 50 %

.. autosummary:: cmrseq.bausteine

Summary
--------------------------------------------------

.. autosummary::	cmrseq.bausteine.SequenceBaseBlock
.. autosummary::	cmrseq.bausteine.Gradient
.. autosummary::	cmrseq.bausteine.TrapezoidalGradient
.. autosummary::	cmrseq.bausteine.ArbitraryGradient
.. autosummary::	cmrseq.bausteine.RFPulse
.. autosummary::	cmrseq.bausteine.SincRFPulse
.. autosummary::	cmrseq.bausteine.ArbitraryRFPulse
.. autosummary::	cmrseq.bausteine.ADC
.. autosummary::	cmrseq.bausteine.SymmetricADC
.. autosummary::	cmrseq.bausteine.GridSamplingADC
.. autosummary::	cmrseq.bausteine.Delay


.. toctree::
   :maxdepth: 2
   :hidden:

   cmrseq.bausteine.general
   cmrseq.bausteine.ADC
   cmrseq.bausteine.gradients
   cmrseq.bausteine.rf
