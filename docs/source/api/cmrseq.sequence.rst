Sequence
===================

**Import:** cmrseq.Sequence

.. autoclass:: cmrseq.core._sequence.Sequence
   :members:
   :undoc-members:
   :show-inheritance:
