Contribution
==============

.. automodule:: cmrseq.contrib

.. toctree::
    :maxdepth: 1
    :hidden:

    cmrseq.contrib.sequences
