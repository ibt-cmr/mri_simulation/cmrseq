Sampling (ADC)
===================

.. automodule:: cmrseq.core.bausteine._adc
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: bysource

.. automodule:: cmrseq.core.bausteine._delay
   :members: