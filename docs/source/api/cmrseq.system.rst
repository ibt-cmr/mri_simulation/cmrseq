SystemSpec
===================

**Import:** cmrseq.SystemSpec

.. autoclass:: cmrseq.core._system.SystemSpec
   :members:
   :undoc-members:
   :show-inheritance:
