Base
===================

.. autoclass:: cmrseq.core.bausteine._base.SequenceBaseBlock
   :members:
   :undoc-members:
   :show-inheritance:
