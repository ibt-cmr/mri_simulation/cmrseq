OMatrix
===================

**Import:** cmrseq.OMatrix

.. autoclass:: cmrseq.core._omatrix.OMatrix
   :members:
   :undoc-members:
   :show-inheritance:
