API reference
==============

Functionality of cmrseq is grouped into several submodules, given as an overview below. All
package import path are as follow:

- cmrseq
- cmrseq.SystemSpec
- cmrseq.Sequence
- cmrseq.OMatrix
- cmrseq.bausteine
- cmrseq.seqdefs
- cmrseq.contrib
- cmrseq.io
- cmrseq.plotting
- cmrseq.utils

Core functionality for defining system specifications, composing sequences and creating blocks is
contained in the classes SystemSpec, Sequence and bausteine. Predefined, parametric composited
definitions are grouped into the seqdefs module. Contributions of composite definitions,
that are not integrated into the main package, due to insufficient testing or unclear use are kept
in the contrib module. Plotting, IO and auxiliary utility is separated into the corresponding packages.

.. image:: ../_static/Overview.svg
   :scale: 40 %

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.sequence
   cmrseq.system
   cmrseq.omatrix
   cmrseq.bausteine

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.seqdefs

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.contrib

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.io

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.plotting

.. toctree::
   :maxdepth: 1
   :hidden:

   cmrseq.utils



