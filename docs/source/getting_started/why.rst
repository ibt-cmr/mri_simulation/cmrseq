Why CMRseq?
============

1. Intuitive Sequence Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The building blocks in CMRseq are mutable objects that are contained by a Sequence object.
Therefore, definitions of complex MR-experiments follow the semantics of talking about MR-sequences,
e.g. :

   *"The diffusion decoding starts at the end of the slice selective refocusing pulse"*

The diffusion decoding block can be instantiated and shifted to match the timing of the
existing trapezoidal slice-selection block.

   *"The area of the phase-encoding trapezoidal decreases per TR in the GRE sequence while
   the timing is constant"*

The shortest timing is determined by instantiating a trapezoidal with maximal encoding. All other
blocks can be created by copying the initial one and scaling it with the corresponding factor


2. Consistent Physical Units
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To improve the readability and consistency of sequence definitions, cmrseq uses the pint package
which allows wrapping all quantities with a physical unit. Dimensionality analysis is performed
on every calculation involving these quantities, which increases the consistency of definitions and
subsequent calculations


3. Documentation, Unit-testing & Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
CMRseq offers a extensive documentation to the presented functionality and a structured display
of the underlying assumption. Also, the project contains unit-tests to ensure the correctness for
the stated assumption and reduce the risk of unintentionally breaking compatibility on extending
the package. The tests are run before every merge into the main version and a proper versioning
is established. Finally, the written code aims at following the PEP styling guide-lines, which is
evaluated by PyLint.
