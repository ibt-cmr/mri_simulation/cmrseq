Getting Started
=======================

.. toctree::
    :hidden:
    :caption: Set up

    why
    authors
    installation

Set up
-------------

.. grid:: 2

    .. grid-item-card:: Why CMRseq?
        :link: why
        :link-type: doc

        Learn more about the idea behind CMRseq.

    .. grid-item-card::  Authors & Citation
        :link: authors
        :link-type: doc

        Using CMRseq in your research? Please consider citing or
        acknowledging us.


.. grid:: 1

    .. grid-item-card::  Installation
        :link: installation
        :link-type: doc

        CMRsqm offers a pip-installable python package.
        For more information checkout the installation page


Start working with CMRsim
-------------------------------------

Introductory Notebooks
^^^^^^^^^^^^^^^^^^^^^^^^^^

In addition to the introduction-scenarios the following note-books provide examples of using cmrseq
features ordered by type of functionality:

.. card-carousel:: 2

    .. card:: Basic Functionality

        .. nbgallery::
            :caption: Introductory Notebooks

            construct_sequence

    .. card:: Sequence Composition

        .. nbgallery:: combining_sequence

    .. card:: Gridding

        .. nbgallery:: sequence_gridding

    .. card:: OMatrix

        .. nbgallery:: orientation_matrix

    .. card:: Parametric Definitions

        .. nbgallery:: seqdef_demo

    .. card:: Import/Export

        .. nbgallery:: io_module

Introduction Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^

.. card-carousel:: 1

    .. card:: SE-M012-SSEPI

        Click here to get an introduction on how to use cmrseq by following a scenario of defining
        a diffusion weighted spin-echo sequence with a single-shot epi readout.

        .. nbgallery::
            :caption: Introduction Scenarios

            introduction_scenario_1
