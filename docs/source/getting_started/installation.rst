Installation & Requirements
-----------------------------

pip - install
^^^^^^^^^^^^^^

CMRsim can be installed using the python package manager :code:`pip`. The package can be found
in the `registry`_  of the repository hosted on the ETH-Gitlab instance as well as on the python
package index (PyPI).

.. _registry: https://gitlab.ethz.ch/jweine/cmrsim/-/packages

**Versioning:**
Stable release versions are denoted with the ususal :code:`major.minor` notation. Additionally,
development versions are constantly updated on solved issued and can be installed using the
:code:`major.minor.devX` notation

Release versions are published on PyPI, which allows installing cmrseq with:


.. code-block::

    pip install cmrseq

Development versions are only available using the extra url:

.. code-block::

    pip install cmrseq --extra-index-url https://gitlab.ethz.ch/api/v4/projects/30300/packages/pypi/simple

Complete pip-resolvable dependencies for cmrseq are automatically installed and given as:

.. code-block::

    python>=3.10
    numpy>=1.19
    pint>=0.18
    matplotlib>=3.5
    scipy>=1.9.3

`pint`_ is python package implementing physical units for numpy arrays

.. _pint: https://github.com/hgrecco/pint


Recommended environment
^^^^^^^^^^^^^^^^^^^^^^^^^
To have the best experience while using cmrseq, we recommend to use Jupyter-lab including the
interactive widgets. For more information look at the following resources:

- https://jupyterlab.readthedocs.io/en/stable/
- https://ipywidgets.readthedocs.io/en/stable/
