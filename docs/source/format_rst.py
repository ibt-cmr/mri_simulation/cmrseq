import cmrseq
import pkgutil


def format_bausteine():
    header = f"\nSummary\n{'-' * 50}\n\n"
    body = [f".. autosummary::\tcmrseq.bausteine.{name}" for name in cmrseq.bausteine.__all__]
    body = header + "\n".join(body) + "\n\n"
    print(body)

    with open("api/cmrseq.bausteine.rst", "r+") as filep:
        content = filep.readlines()
        content.insert(5, body)
        filep.seek(0)
        content = "".join(content)
        filep.write(content)


if __name__ == "__main__":
    format_bausteine()