.. cmrseq documentation master file, created by
   sphinx-quickstart on Thu Jun 16 10:53:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. carousel::

   .. image:: _static/banner1.svg

------------------------------------------------------------------------------------------------


Welcome to cmrseq!
==================================

**Define your MRI sequences in pure python!**

The cmrseq frame-work is build to define MRI sequences consisting of radio-frequency pulses,
gradient waveforms and sampling events. All definitions follow the concept hierarchically assemble
experiments where the basic building blocks (Arbitrary Gradients, Trapezoidals, RF-pulses and
ADC-events) are forming the core functionality and are all instances of SequenceBaseBlock-instances.
On instantiation all base-blocks are validated against the System specifications. Composition of
base-blocks is done in a Sequence object. The Sequence object implements convenient definitions
for addition and composition of multiple Sequence objects as well as to perform a variety on common
operations on semantically grouped base-blocks.

Several semantically connected groups of building blocks (e.g. a slice selective excitation) are
allready functionally defined in the parametric_definitions module. For a complete list of available
definitions checkout the API-reference.

The original motivation for cmrseq was to create a foundation to define sequences for simulation
experiments. Therefore Sequences can be easily gridded onto a regular (or even unregular grids with
a maximum step width) grids. Furthermore, commonly useful functionalities as plotting, evaluation
of k-space-trajectories, calculation of moments, etc.

To close the gap to real-world measurements, cmrseq includes an IO module that allows loading
Phillips (GVE) sequence definitions as well as reading and writing Pulseq (>= 1.4) files, which
then can be used to export the sequence to multiple vendor platforms. For more information on this
file format please refer to the official `Pulseq web-page`_.

.. _Pulseq web-page: https://pulseq.github.io/


.. image:: _static/logo_cmrseq.svg
   :class: only-light
   :align: right
   :scale: 150 %

.. image:: _static/logo_cmrseq_dark.svg
   :class: only-dark
   :align: right
   :scale: 150 %

**Awesome features you only get with cmrseq**:
   - Actual physical units for all definitions
   - Proper documentation & tested software
   - Modularity of sequence definition and modification of building blocks.

For Instructions on installation and a detailed introdction to the cmrseq-keyconcepts as well as
examples checkout the :ref:`getting started/index` page.

.. toctree::
   :maxdepth: 1
   :hidden:

   getting_started/index
   api/cmrseq


Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
