# Documentation build configuration for cmrseq

# -- Project information -----------------------------------------------------

project = 'cmrseq'
copyright = '2022, Jonathan Weine & Charles McGrath'
author = 'Jonathan Weine & Charles McGrath'

import sys
from typing import Any
import os
from bs4 import BeautifulSoup
sys.path.append(f"{os.path.dirname(os.path.dirname(os.path.dirname(__file__)))}")
import cmrseq

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = [#'pydata_sphinx_theme',
              'sphinx.ext.todo',
              'sphinx.ext.viewcode',
              'sphinx.ext.autodoc',
              'sphinx.ext.autosummary',
              'sphinx.ext.mathjax',
              'autodocsumm',
              'sphinx_autodoc_typehints',
              'IPython.sphinxext.ipython_console_highlighting',
              'sphinx_carousel.carousel',
              'sphinx_design',
              'nbsphinx',
              'sphinx_gallery.load_style',  # load CSS for gallery (needs SG >= 0.6)
              'sphinx.ext.intersphinx'
              ]

html_theme = 'pydata_sphinx_theme'
autodoc_typehints = 'description'
autodoc_typehints_format = 'short'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []
add_module_names = False

mathjax3_config = {
    'tex': {'tags': 'ams', 'useLabelIds': True},
}

# -- Options for HTML output -------------------------------------------------


# Define the version we use for matching in the version switcher.
json_url = "https://people.ee.ethz.ch/~jweine/cmrseq/latest/_static/switcher.json"
version_match = os.environ.get("CMRSEQ_BUILD_VERSION")
if not version_match:
    release = cmrseq.__version__
    if "dev" in release:
        version_match = "dev"
    else:
        version_match = "v" + release

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

add_module_name = False

html_static_path = ['_static']
html_css_files = ['css/custom.css', ]

html_theme_options = {
    "show_prev_next": False,
    "show_nav_level": 2,
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.ethz.ch/ibt-cmr/mri_simulation/cmrseq",
            "icon": "_static/gitlab-logo-100.svg",
            "type": "local",
        }],
    "navbar_align": "content",
    "navbar_start": ["navbar-logo", "version-switcher"],
    "navbar_center": ["navbar-nav"],
    "secondary_sidebar_items": ["class_page_toc"],
    "logo": {
        "image_light": "_static/logo_cmrseq.svg",
        "image_dark": "_static/logo_cmrseq_dark.svg",
        "text": "cmrseq",
        "scale": "160%"
    },
    "switcher": {
        "json_url": "https://people.ee.ethz.ch/~jweine/cmrseq/latest/_static/switcher.json",
        "version_match": version_match,
        },
}

autodoc_default_options = {
    'autosummary': True,
    'special-members': '__call__',
    'members': True,
    'attributes': True,
    'properties': True,
    'show_inheritance': True,
    'inherited-members': False,
    'member-order': "groupwise",
    'imported-members': False,
    'autoclass_content': 'both',
    'autodoc_class_signature': 'mixed',
    'autodoc_typehints':"description"
}

nbsphinx_thumbnails = {
    # Default
    'getting_started/*': '_static/logo_cmrseq.svg',
}



def _setup_navbar_side_toctree(app: Any):

    def add_class_toctree_function(app: Any, pagename: Any, templatename: Any,
                                   context: Any, doctree: Any):
        def get_class_toc() -> Any:
            try:
                soup = BeautifulSoup(context["body"], "html.parser")

                matches = soup.find_all('dl')
                if matches is None or len(matches) == 0 or isinstance(matches[0], str):
                    return ""
                items = []
                deeper_depth = matches[0].find('dt').get('id').count(".")
                for match in matches:
                    match_dt = match.find('dt')
                    if match_dt is not None and match_dt.get('id') is not None:
                        current_depth = match_dt.get('id').count(".")
                        current_link = match.find(class_="headerlink")
                        current_title = match_dt.get('id').split(".")[-1]
                        current_title = current_title

                        if current_link is not None:
                            if deeper_depth > current_depth:
                                deeper_depth = current_depth
                            if deeper_depth == current_depth:
                                 items.append({
                                    "title": current_title,
                                    "link": current_link["href"],
                                    "attributes_and_methods": []
                                })
                            if deeper_depth < current_depth:
                                items[-1]["attributes_and_methods"].append(
                                    {
                                        "title": "--" + current_title,
                                        "link": current_link["href"],
                                    }
                                )
                return items
            except:
                return ""

        context["get_class_toc"] = get_class_toc

    app.connect("html-page-context", add_class_toctree_function)

def setup(app: Any):
    _setup_navbar_side_toctree(app)
