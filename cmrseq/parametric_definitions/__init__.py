# pylint: disable=R0801
""" This module contains parametric definition of building-block compositions sorted by their
application"""
__all__ = ["diffusion", "velocity", "readout", "sequences", "excitation", "preparation"]

from cmrseq.parametric_definitions import diffusion
from cmrseq.parametric_definitions import velocity
from cmrseq.parametric_definitions import readout
from cmrseq.parametric_definitions import sequences
from cmrseq.parametric_definitions import excitation
from cmrseq.parametric_definitions import preparation
