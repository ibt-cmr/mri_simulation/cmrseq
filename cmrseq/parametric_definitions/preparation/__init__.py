"""Submodule containing MR-preparation pulses"""
__all__ = ["spinlock"]

import cmrseq.parametric_definitions.preparation._spinlock_prepulse as spinlock
